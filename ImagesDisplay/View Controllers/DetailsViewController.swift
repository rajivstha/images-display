//
//  DetailsViewController.swift
//  ImagesDisplay
//
//  Created by Rajiv Shrestha on 5/16/19.
//  Copyright © 2019 CE. All rights reserved.
//

import UIKit
import SDWebImage

class DetailsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet var img_Display: UIImageView!
    @IBOutlet var txt_ImageID: UILabel!
    @IBOutlet var txt_ImageBy: UILabel!
    @IBOutlet var txt_usrLink: UILabel!
    @IBOutlet var txt_imgCount: UILabel!
    @IBOutlet var txt_MoreImages: UILabel!
    @IBOutlet weak var usrImgCollView: UICollectionView!
    
    var data_Source: ImageSourceModel?
    var Passed_imglink:String?
    var passed_sourceID:Int?
    var passed_imgID:Int?
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let url = URL(string: (Passed_imglink ?? ""))
        img_Display.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        getSourceData()
        customDisplayCells()
        let sourceID = "\(passed_sourceID ?? 0)"
        if sourceID == "0"{
            txt_MoreImages.isHidden = true
            usrImgCollView.isHidden = true
        }
    }

    
    // fetch the image sources
    func getSourceData(){
        let sourceID = "\(passed_sourceID ?? 0)"
        let sourceImgUrl = String(format: "%@%@%@",baseUrl,sourceUrl,sourceID)
        print(sourceImgUrl)
        if sourceID != "0"{
        apiServices.getSource(sourceImgUrl, success: { (sourceData) in
            self.data_Source = sourceData
            DispatchQueue.main.async {
                self.fillUsrDet()
                self.usrImgCollView.reloadData()
            }
        }) { (errorMessage) in
            AlertControl.alertControl(globleAlert: self, DialogTitle: errorMessage as NSString, strDialogMessege: "")
        }
        }else{
            self.fillUsrDet()
            self.usrImgCollView.reloadData()
        }
    }
    
    // fill user details
    func fillUsrDet(){
        let sourceID = "\(passed_sourceID ?? 0)"
        if sourceID != "0"{
        txt_ImageBy.text = data_Source?.name
        txt_ImageID.text = "\(passed_imgID ?? 0)"
        txt_usrLink.text = data_Source?.url
        txt_imgCount.text = "\(data_Source?.image_count ?? 0)"
        }else{
            txt_ImageBy.text = "N/A"
            txt_ImageID.text = "\(passed_imgID ?? 0)"
            txt_usrLink.text = "N/A"
            txt_imgCount.text = "N/A"
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data_Source?.images.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! UserImagesCollectionViewCell
        
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.lightGray.cgColor
        
        let imageList = data_Source?.images[indexPath.row]
        let url = URL(string: (imageList?.url ?? ""))
        cell.img_Default.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageList = data_Source?.images[indexPath.row]
        Passed_imglink = imageList?.url
        passed_sourceID = imageList?.source_id
        passed_imgID = imageList?.id
        viewWillAppear(true)
    }
    
    // Setting up the Views for Different Devices
    func customDisplayCells(){
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        
        if (GlobalConstants.DeviceType.IS_IPAD || GlobalConstants.DeviceType.IS_IPAD_PRO){
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
            layout.itemSize = CGSize(width: usrImgCollView.bounds.size.width/1.7, height: usrImgCollView.bounds.size.height/2.5)
            layout.minimumInteritemSpacing = 5
            layout.minimumLineSpacing = 5
            layout.scrollDirection = .horizontal
            usrImgCollView!.collectionViewLayout = layout
        }else if GlobalConstants.DeviceType.IS_IPHONE_5{
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
            layout.itemSize = CGSize(width: usrImgCollView.bounds.size.width/2.85, height: usrImgCollView.bounds.size.height/3.6)
            layout.minimumInteritemSpacing = 5
            layout.minimumLineSpacing = 5
            layout.scrollDirection = .horizontal
            usrImgCollView!.collectionViewLayout = layout
        }else{
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
            layout.itemSize = CGSize(width: usrImgCollView.bounds.size.width/2.15, height: usrImgCollView.bounds.size.height/3)
            layout.minimumInteritemSpacing = 5
            layout.minimumLineSpacing = 5
            layout.scrollDirection = .horizontal
            usrImgCollView!.collectionViewLayout = layout
        }
    }
}
