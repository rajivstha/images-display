//
//  ViewController.swift
//  ImagesDisplay
//
//  Created by Rajiv Shrestha on 5/15/19.
//  Copyright © 2019 CE. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate {

    var topImages : [LatestImagesPair]?
    var searchImages : [SearchPair]?
    
    @IBOutlet weak var imagesCollView: UICollectionView!
    @IBOutlet weak var txt_SearchQuery: UISearchBar!
    
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    var searching:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        getTopImages()
        customDisplayCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getTopImages()
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if !searching {
            return topImages?.count ?? 0
        }else{
            if searchImages?.count == 0 {
                AlertControl.alertControl(globleAlert: self, DialogTitle: "Oops!!", strDialogMessege: "No Data Found.. Press 'Cancel' on Search Bar to go back")
            }
            return searchImages?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if !searching {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCollectionViewCell
        
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.lightGray.cgColor
        
        let imageList = topImages?[indexPath.row]
        let url = URL(string: (imageList?.url ?? ""))
        cell.img_Default.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        
        return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCollectionViewCell
            
            cell.layer.borderWidth = 1
            cell.layer.borderColor = UIColor.lightGray.cgColor
            
            let imageList = searchImages?[indexPath.row]
            let url = URL(string: (imageList?.url ?? ""))
            cell.img_Default.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
            
            return cell
        }
    }
    
    //Clicking on the Images
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !searching {
            let VC = storyboard?.instantiateViewController(withIdentifier: "DetailsPage") as! DetailsViewController
            let imageList = topImages?[indexPath.row]
            VC.Passed_imglink = imageList?.url
            VC.passed_sourceID = imageList?.source_id
            VC.passed_imgID = imageList?.id
            self.navigationController?.pushViewController(VC, animated: true)
        }else{
            let VC = storyboard?.instantiateViewController(withIdentifier: "DetailsPage") as! DetailsViewController
            let imageList = searchImages?[indexPath.row]
            VC.Passed_imglink = imageList?.url
            VC.passed_sourceID = imageList?.source_id
            VC.passed_imgID = imageList?.id
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    //Fetching the Data of Latest Images
    func getTopImages(){
        apiServices.getLatestImages(success: { (latestImages) in
            self.topImages = latestImages
             DispatchQueue.main.async{
            self.imagesCollView.reloadData()
            }
        }) { errorMessage in
            AlertControl.alertControl(globleAlert: self, DialogTitle: errorMessage as NSString, strDialogMessege:"" )
        }
    }
    
    //Fetching the Data of Search Result
    func getSearchImages(){
        let searchQuery = txt_SearchQuery.text
        let imageSearchUrl = String(format: "%@%@%@",baseUrl,searchUrl,searchQuery ?? "")
        apiServices.getSearchList(imageSearchUrl, success: { (searchList) in
            self.searchImages = searchList
            self.searching = true
             DispatchQueue.main.async{
                self.imagesCollView.reloadData()
            }
        }) { (errorMessage) in
            AlertControl.alertControl(globleAlert: self, DialogTitle: errorMessage as NSString, strDialogMessege: "")
        }
    }
    
    //When Cancel button is clicked on Search Bar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        DispatchQueue.main.async {
            self.imagesCollView.reloadData()
        }
    }
    
    // When Return is pressed while searching
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        getSearchImages()
    }
    

    // Setting up the Views for Different Devices
    func customDisplayCells(){
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        
        if (GlobalConstants.DeviceType.IS_IPAD || GlobalConstants.DeviceType.IS_IPAD_PRO){
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
            layout.itemSize = CGSize(width: imagesCollView.bounds.size.width/1.7, height: imagesCollView.bounds.size.height/3.25)
            layout.minimumInteritemSpacing = 5
            layout.minimumLineSpacing = 5
            imagesCollView!.collectionViewLayout = layout
        }else if GlobalConstants.DeviceType.IS_IPHONE_5{
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
            layout.itemSize = CGSize(width: imagesCollView.bounds.size.width/2.85, height: imagesCollView.bounds.size.height/5.5)
            layout.minimumInteritemSpacing = 5
            layout.minimumLineSpacing = 5
            imagesCollView!.collectionViewLayout = layout
        }else{
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
            layout.itemSize = CGSize(width: imagesCollView.bounds.size.width/2.15, height: imagesCollView.bounds.size.height/4.2)
            layout.minimumInteritemSpacing = 5
            layout.minimumLineSpacing = 5
            imagesCollView!.collectionViewLayout = layout
        }
    }
}


