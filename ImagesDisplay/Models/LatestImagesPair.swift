//
//  LatestImagesPair.swift
//  ImagesDisplay
//
//  Created by Rajiv Shrestha on 5/15/19.
//  Copyright © 2019 CE. All rights reserved.
//

import Foundation

struct LatestImagesPair: Codable {
    
    var id: Int?
    var url: String?
    var large_url: String?
    var source_id: Int?
    
}
