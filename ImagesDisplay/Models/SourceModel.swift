//
//  SourceModel.swift
//  ImagesDisplay
//
//  Created by Rajiv Shrestha on 5/16/19.
//  Copyright © 2019 CE. All rights reserved.
//

import Foundation

struct ImageSourceModel: Codable {
    var id: Int?
    var name: String?
    var url: String?
    var image_count: Int?
    var images: [LatestImagesPair]
}
