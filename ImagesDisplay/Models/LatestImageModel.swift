//
//  LatestImageModel.swift
//  ImagesDisplay
//
//  Created by Rajiv Shrestha on 5/15/19.
//  Copyright © 2019 CE. All rights reserved.
//

import Foundation

struct ImageModel: Codable{
    
    var images: [LatestImagesPair]
}
