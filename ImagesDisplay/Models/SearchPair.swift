//
//  SearchPair.swift
//  ImagesDisplay
//
//  Created by Rajiv Shrestha on 5/15/19.
//  Copyright © 2019 CE. All rights reserved.
//

import Foundation

struct SearchPair: Codable {
    
    var id: Int?
    var url: String?
    var large_url: String?
    var source_id: Int?
    var copyright: String?
    var site: String?
    
}
