//
//  APIUrls.swift
//  ImagesDisplay
//
//  Created by Rajiv Shrestha on 5/15/19.
//  Copyright © 2019 CE. All rights reserved.
//

import Foundation

let baseUrl = "http://www.splashbase.co"

let latestImagesUrl = "/api/v1/images/latest"

let searchUrl = "/api/v1/images/search?query="

let sourceUrl = "/api/v1/sources/"

