//
//  AlertControl.swift
//  ImagesDisplay
//
//  Created by Rajiv Shrestha on 5/15/19.
//  Copyright © 2019 CE. All rights reserved.
//

import Foundation
import UIKit

class AlertControl {
    
    class func alertControl(globleAlert:UIViewController, DialogTitle:NSString, strDialogMessege:NSString){
        
        let actionSheetController: UIAlertController = UIAlertController(title: DialogTitle as String, message: strDialogMessege as String, preferredStyle: .alert)
        
        let nextAction: UIAlertAction = UIAlertAction(title: "OK", style: .default) { action -> Void in
        }
        
        actionSheetController.addAction(nextAction)
        
        globleAlert.present(actionSheetController, animated: true, completion:nil)
        
    }
    
}
