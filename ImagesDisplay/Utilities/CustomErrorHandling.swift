//
//  CustomErrorHandling.swift
//  ImagesDisplay
//
//  Created by Rajiv Shrestha on 5/15/19.
//  Copyright © 2019 CE. All rights reserved.
//

import Foundation
import UIKit

enum CustomError: String, Error {
    case unknownError
    case connectionError
    case invalidCredentials
    case invalidRequest
    case notFound
    case invalidResponse
    case serverError
    case serverUnavailable
    case timeOut
    case unsuppotedURL
}

func checkErrorCode(_ errorCode: Int) -> CustomError {
    switch errorCode {
    case 400:
        return .invalidCredentials
    case 401:
        return .serverError
    case 403:
        return .connectionError
    case 404:
        return .serverUnavailable
    default:
        return .unknownError
    }
}

