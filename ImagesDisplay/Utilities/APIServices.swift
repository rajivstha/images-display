//
//  APIServices.swift
//  ImagesDisplay
//
//  Created by Rajiv Shrestha on 5/15/19.
//  Copyright © 2019 CE. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD

class apiServices{
    
    static func getLatestImages(success: @escaping (_ latestImages: [LatestImagesPair]) -> (), failure: @escaping (_ errorMessage: String)  -> () ) {
        
        let headers = [
            "content-type":"application/json",
            "accept":"application/json"
        ]
        
        let getTopImagesURL = String(format: "%@%@",baseUrl,latestImagesUrl)
        SVProgressHUD.show()

        Alamofire.request(getTopImagesURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseString { (response) in
            let statusCode = response.response?.statusCode
            
            var topImages : [LatestImagesPair]?
            
            switch response.result{
            case.success(let value):
                if statusCode == 200 {
                    let jsonString = value
                    
                    if let jsonData = jsonString.data(using: .utf8){
                        let imageObject = try? JSONDecoder().decode(ImageModel.self, from: jsonData)
                        guard let Images = imageObject else{
                            return
                        }
                        topImages = Images.images
                    }
                    guard let latestTopImages = topImages else{
                        return
                    }
                    success(latestTopImages)
                    SVProgressHUD.dismiss()
                }else{
                    SVProgressHUD.dismiss()
                    let error = checkErrorCode(statusCode ?? 400)
                    return failure(error.rawValue)
                }
            case.failure( _):
                SVProgressHUD.dismiss()
                let customerror = checkErrorCode(statusCode ?? 404)
                failure(customerror.rawValue)
            }}
    }
    
    static func getSearchList(_ path: String, success: @escaping (_ imagesSearch: [SearchPair]) -> (), failure: @escaping(_ errorMessage: String) -> () ){
        
        let headers = [
            "content-type":"application/json",
            "accept":"application/json"
        ]
        SVProgressHUD.show()
        Alamofire.request(path, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseString { (response) in
            let statusCode = response.response?.statusCode
            
            var searchImages: [SearchPair]?
            
            switch response.result{
            case .success(let value):
                if (statusCode == 200){
                    let jsonString = value
                    if let jsonData = jsonString.data(using: .utf8)
                    {
                        let searchObject = try? JSONDecoder().decode(SearchModel.self, from: jsonData)
                        guard let searchData = searchObject else {
                            return
                        }
                        searchImages = searchData.images
                    }
                    guard let newSearchList = searchImages else{
                        return
                    }
                    success(newSearchList)
                    SVProgressHUD.dismiss()
                }else{
                    SVProgressHUD.dismiss()
                    let error = checkErrorCode(statusCode ?? 400)
                    return failure(error.rawValue)
                }
            case .failure( _):
                SVProgressHUD.dismiss()
                let customerror = checkErrorCode(statusCode ?? 404)
                failure(customerror.rawValue)
            }}
    }
    
    static func getSource(_ path: String, success: @escaping (_ sourceImg: ImageSourceModel) -> (), failure: @escaping(_ errorMessage: String) -> () ){
        
        let headers = [
            "content-type":"application/json",
            "accept":"application/json"
        ]
        SVProgressHUD.show()
        Alamofire.request(path, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseString { (response) in
            let statusCode = response.response?.statusCode
            
            var imgSource: ImageSourceModel?
            
            switch response.result{
            case .success(let value):
                if (statusCode == 200){
                    let jsonString = value
                    if let jsonData = jsonString.data(using: .utf8)
                    {
                        let sourceObject = try? JSONDecoder().decode(ImageSourceModel.self, from: jsonData)
                        guard let sourceData = sourceObject else {
                            return
                        }
                        imgSource = sourceData
                    }
                    guard let newSource = imgSource else{
                        return
                    }
                    success(newSource)
                    SVProgressHUD.dismiss()
                }else{
                    SVProgressHUD.dismiss()
                    let error = checkErrorCode(statusCode ?? 400)
                    return failure(error.rawValue)
                }
            case .failure( _):
                SVProgressHUD.dismiss()
                let customerror = checkErrorCode(statusCode ?? 404)
                failure(customerror.rawValue)
            }}
    }
}
